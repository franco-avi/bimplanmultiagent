package island;

public class Constants {

    // public static final String DATASET = "/Users/Franco/IdeaProjects/ResearchProject/src/main/resources/chain.csv";
    public static final String DATASET = "/Users/Franco/IdeaProjects/ResearchProject/src/main/resources/industrial.csv";

    public final static int POPULATION_SIZE = 10;
    public final static int GENERATION_LIMIT = 1;

    public final static int TOURNAMENT_SIZE = 5;
    public final static int ELITISM_SIZE = 1;

    public final static double MIGRATION_PROB = 0.05;

    public static final int ISLAND_NUMBER = 10;
    public static final double BRIDGE_PROBABILITY = 1.0;

    public static final int MAX_ROUND = 5;
}
