package island.messages;

import java.io.Serializable;

public class EvolutionStart implements Serializable {

    private final boolean hasToBePrinted;

    public EvolutionStart(boolean lastRound) {
        this.hasToBePrinted = lastRound;
    }

    public boolean hasToBePrinted() {
        return hasToBePrinted;
    }
}
