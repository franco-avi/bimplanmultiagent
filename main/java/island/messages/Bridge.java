package island.messages;

import akka.actor.ActorRef;

import java.io.Serializable;

public class Bridge implements Serializable {

    private final ActorRef island;

    public Bridge(ActorRef bridge) {
        this.island = bridge;
    }

    public ActorRef getIsland() {
        return island;
    }
}
