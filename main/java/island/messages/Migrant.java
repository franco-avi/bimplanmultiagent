package island.messages;

import jenes.chromosome.DoubleChromosome;
import jenes.population.Individual;

import java.io.Serializable;

public class Migrant implements Serializable {

    private final Individual<DoubleChromosome> individual;

    public Migrant(Individual<DoubleChromosome> individual) {
        this.individual = individual;
    }

    public Individual<DoubleChromosome> getIndividual() {
        return individual;
    }
}
