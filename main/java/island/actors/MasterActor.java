package island.actors;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import island.Constants;
import island.MainIsland;
import island.messages.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static island.Constants.*;

public class MasterActor extends AbstractActor {


    private List<ActorRef> islands = new ArrayList<>();
    private int counter;
    private int round;

    public static Props props(){
        return Props.create(MasterActor.class);
    }


    public AbstractActor.Receive createReceive() {
        ReceiveBuilder receiveBuilder = receiveBuilder();
        receiveBuilder.match(EvolutionEnd.class, this::endEvolution);

        return receiveBuilder.build();
    }

    @Override
    public void preStart() {
        for (int i = 0; i < Constants.ISLAND_NUMBER; i++) {
            ActorRef island = getContext().actorOf(IslandActor.props());
            islands.add(island);
        }

        Random rand = new Random();
        for (int i = 0; i < ISLAND_NUMBER; i++) {
            for (int j = 0; j < ISLAND_NUMBER; j++) {
                if (i != j && rand.nextDouble() <= BRIDGE_PROBABILITY) {
                    islands.get(i).tell(new Bridge(islands.get(j)), ActorRef.noSender());
                }
            }
        }

        for (ActorRef island : islands) {
            island.tell(new EvolutionInit(), getSelf());
        }
    }

    private void endEvolution(EvolutionEnd msg){
        counter++;
        if (counter >= islands.size()){
            round++;
            counter = 0;

            if (round <= MAX_ROUND) {
                System.out.println("Round completed: " + round);

                for (ActorRef island : islands) {
                    island.tell(new EvolutionStart(round == MAX_ROUND), getSelf());
                }
            } else {
                System.out.println(System.currentTimeMillis() - MainIsland.time);
                getContext().system().terminate();
            }
        }
    }
}
