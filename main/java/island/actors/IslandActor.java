package island.actors;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import island.messages.*;
import jenes.GeneticAlgorithm;
import jenes.chromosome.DoubleChromosome;
import jenes.population.Fitness;
import jenes.population.Individual;
import jenes.population.Population;
import jenes.stage.AbstractStage;
import jenes.stage.operator.common.OnePointCrossover;
import jenes.stage.operator.common.TournamentSelector;
import taskgraph.TaskGraph;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static island.Constants.*;

public class IslandActor extends AbstractActor {

    private final TaskGraph taskGraph = new TaskGraph(DATASET);
    private final List<ActorRef> islands = new ArrayList<>();

    private Population<DoubleChromosome> population;
    private List<Individual<DoubleChromosome>> migrants = new ArrayList<>();

    private ActorRef master;

    private Random rand = new Random();

    public static Props props() {
        return Props.create(IslandActor.class);
    }

    public Receive createReceive() {
        ReceiveBuilder receiveBuilder = receiveBuilder();

        receiveBuilder.match(EvolutionInit.class, this::initEvolution);
        receiveBuilder.match(EvolutionStart.class, this::startEvolution);
        receiveBuilder.match(Bridge.class, this::addBridge);
        receiveBuilder.match(Migrant.class, this::addMigrant);

        return receiveBuilder.build();
    }

    private void initEvolution(EvolutionInit msg) {
        master = getSender();

        DoubleChromosome chromosome = new DoubleChromosome(taskGraph.getTaskNumber(), 0.0, 1.0);
        Individual<DoubleChromosome> sample = new Individual<>(chromosome);
        population = new Population<>(sample, POPULATION_SIZE);

        startEvolution(new EvolutionStart(false));
    }

    private void startEvolution(EvolutionStart msg) {
        mergeMigrants();
        evolveIsland();

        List<Individual<DoubleChromosome>> migrants = getMigrants();
        for (Individual<DoubleChromosome> migrant : migrants) {
            ActorRef island = islands.get(rand.nextInt(islands.size()));
            island.tell(new Migrant(migrant), getSelf());
        }

        if (msg.hasToBePrinted()) {
            Population.Statistics stats = population.getStatistics();
            Individual best = stats.getGroup(Population.BEST).get(0);
            System.out.printf("SCORE: %f, PLAN: %s\n", best.getScore(), taskGraph.getSchedule(best));
        }

        master.tell(new EvolutionEnd(), getSelf());
    }

    private void addBridge(Bridge bridgeMsg) {
        islands.add(bridgeMsg.getIsland());
    }

    private void addMigrant(Migrant migrantMsg) {
        migrants.add(migrantMsg.getIndividual());
    }

    private void evolveIsland() {
        GeneticAlgorithm<DoubleChromosome> ga = new GeneticAlgorithm<>(population, GENERATION_LIMIT);
        Fitness<DoubleChromosome> fit = new Fitness<DoubleChromosome>(1, false) {

            @Override
            public void evaluate(Individual<DoubleChromosome> individual) {
                double[] score = new double[1];
                score[0] = taskGraph.getSchedule(individual).getFinishTime();
                individual.setScore(score);
            }
        };

        AbstractStage<DoubleChromosome> selector = new TournamentSelector<>(TOURNAMENT_SIZE);
        AbstractStage<DoubleChromosome> crossover = new OnePointCrossover<>(0.85);

        ga.setFitness(fit);
        ga.addStage(selector);
        ga.addStage(crossover);
        ga.setElitism(ELITISM_SIZE);

        ga.evolve();

        population = ga.getLastPopulation();
    }

    private void mergeMigrants() {
        population.add(migrants);
        migrants.clear();
    }

    private List<Individual<DoubleChromosome>> getMigrants() {
        List<Individual<DoubleChromosome>> selection = new ArrayList<>();

        int migrantsCounter = 0;
        for (Individual<DoubleChromosome> individual : population) {
            if (rand.nextDouble() <= MIGRATION_PROB && population.size() - migrantsCounter > 2) {
                selection.add(individual);
                migrantsCounter++;
            }
        }
        return selection;
    }
}

