package island;

import akka.actor.ActorSystem;
import island.actors.MasterActor;

public class MainIsland {

    public static long time = System.currentTimeMillis();

    public static void main(String[] args) {
        ActorSystem system = ActorSystem.create("island-system");
        system.actorOf(MasterActor.props());
    }
}
