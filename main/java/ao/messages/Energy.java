package ao.messages;

import java.io.Serializable;

public class Energy implements Serializable {

    private double energy;

    public Energy(double energy) {
        this.energy = energy;
    }

    public double getEnergy() {
        return energy;
    }
}
