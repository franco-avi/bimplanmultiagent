package ao.messages;

import jenes.chromosome.DoubleChromosome;
import jenes.population.Individual;

public class IndivudalSpawn {

    private Individual<DoubleChromosome> individual;

    public IndivudalSpawn(Individual<DoubleChromosome> individual) {
        this.individual = individual;
    }

    public Individual<DoubleChromosome> getIndividual() {
        return individual;
    }
}
