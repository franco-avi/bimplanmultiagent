package ao.messages;

import java.io.Serializable;

public class Attack implements Serializable {

    private double strength;

    public Attack(double strength) {
        this.strength = strength;
    }

    public double getStrength() {
        return strength;
    }
}
