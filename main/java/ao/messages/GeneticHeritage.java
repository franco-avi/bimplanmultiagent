package ao.messages;

import jenes.chromosome.DoubleChromosome;
import jenes.population.Individual;

import java.io.Serializable;

public class GeneticHeritage implements Serializable {

    private Individual<DoubleChromosome> individual;
    private double strength;

    public GeneticHeritage(Individual<DoubleChromosome> individual, double strength) {
        this.individual = individual;
        this.strength = strength;
    }

    public Individual<DoubleChromosome> getIndividual() {
        return individual;
    }

    public double getStrength() {
        return strength;
    }
}
