package ao.messages;

import akka.actor.ActorRef;

import java.io.Serializable;

public class IndividualRemove implements Serializable {

    private ActorRef individual;

    public IndividualRemove(ActorRef individual) {
        this.individual = individual;
    }

    public ActorRef getIndividual() {
        return individual;
    }
}
