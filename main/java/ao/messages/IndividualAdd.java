package ao.messages;

import akka.actor.ActorRef;

import java.io.Serializable;

public class IndividualAdd implements Serializable {

    private ActorRef individual;

    public IndividualAdd(ActorRef individual) {
        this.individual = individual;
    }

    public ActorRef getIndividual() {
        return individual;
    }
}
