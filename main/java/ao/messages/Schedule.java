package ao.messages;

public class Schedule {

    private int scheduleLength;
    private String schedule;

    public Schedule(int scheduleLength, String schedule) {
        this.scheduleLength = scheduleLength;
        this.schedule = schedule;
    }

    public int getScheduleLength() {
        return scheduleLength;
    }

    public String getSchedule() {
        return schedule;
    }
}
