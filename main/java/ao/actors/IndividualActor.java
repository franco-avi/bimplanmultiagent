package ao.actors;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import ao.messages.*;
import jenes.chromosome.DoubleChromosome;
import jenes.population.Individual;
import jenes.population.Population;
import jenes.stage.operator.Crossover;
import jenes.stage.operator.common.OnePointCrossover;
import taskgraph.TaskGraph;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import static ao.Constants.*;

public class IndividualActor extends AbstractActor {

    private Individual<DoubleChromosome> individual;
    private double energy;

    private TaskGraph taskGraph;
    private Set<ActorRef> neighbours = new HashSet<>();

    private ActorRef master;

    private Random rand = new Random();

    public static Props props(TaskGraph taskGraph) {
        return Props.create(IndividualActor.class, taskGraph);
    }

    public static Props props(Individual<DoubleChromosome> individual, TaskGraph taskGraph) {
        return Props.create(IndividualActor.class, individual, taskGraph);
    }

    public IndividualActor(Individual<DoubleChromosome> individual, TaskGraph taskGraph) {
        this.taskGraph = taskGraph;
        this.individual = individual;
    }

    public IndividualActor(TaskGraph taskGraph) {
        DoubleChromosome randChromosome = new DoubleChromosome(taskGraph.getTaskNumber(), 0.0, 1.0);
        randChromosome.randomize();
        this.individual = new Individual<>(randChromosome);
        this.taskGraph = taskGraph;
    }

    @Override
    public Receive createReceive() {
        ReceiveBuilder receiveBuilder = receiveBuilder();
        receiveBuilder.match(Attack.class, this::takeAttack);
        receiveBuilder.match(Energy.class, this::pickupEnergy);
        receiveBuilder.match(GeneticHeritage.class, this::takeGeneticHeritage);
        receiveBuilder.match(IndividualAdd.class, this::addIndividual);
        receiveBuilder.match(IndividualRemove.class, this::removeIndividual);
        receiveBuilder.match(EvolutionStart.class, this::startEvolution);

        return receiveBuilder.build();
    }

    private void addIndividual(IndividualAdd msg) {
        neighbours.add(msg.getIndividual());
    }

    private void removeIndividual(IndividualRemove msg) {
        neighbours.remove(msg.getIndividual());
    }

    private void startEvolution(EvolutionStart msg) {
        if (master == null) {
            master = getSender();

            double minDuration = taskGraph.getParallelDuration();
            double maxDuration = taskGraph.getSequentialDuration();
            double duration = taskGraph.getSchedule(individual).getFinishTime();

            energy = MAX_START_ENERGY * ((maxDuration - duration) / (maxDuration - minDuration));
        }

        System.out.println(energy);

        if (energy <= DEATH_TRESHOLD && rand.nextDouble() <= DEATH_PROB) {
            //System.out.println("Dying");

            taskgraph.Schedule schedule = taskGraph.getSchedule(individual);
            master.tell(new Schedule(schedule.getFinishTime(), schedule.toString()), getSelf());

            for (ActorRef neighbour : neighbours) {
                neighbour.tell(new IndividualRemove(getSelf()), getSelf());
            }
            getContext().system().stop(getSelf());
        }

        if (energy >= ATTACK_TRESHOLD && rand.nextDouble() <= ATTACK_PROB && neighbours.size() > 0) {
            //System.out.println("Attacking");

            ActorRef victim = (ActorRef) neighbours.toArray()[rand.nextInt(neighbours.size())];
            victim.tell(new Attack(energy), getSelf());
        }

        if (energy >= REPRODUCTION_TRESHOLD && rand.nextDouble() <= REPRODUCTION_PROB && neighbours.size() > 0) {
            //System.out.println("Reproducing");

            ActorRef partner = (ActorRef) neighbours.toArray()[rand.nextInt(neighbours.size())];
            partner.tell(new GeneticHeritage(individual, energy), getSelf());
        }

        getSelf().tell(new EvolutionStart(), getSelf());
    }

    private void takeGeneticHeritage(GeneticHeritage msg) {
        for (int i = 0; i < individual.getChromosomeLength(); i++) {
            Population<DoubleChromosome> parents = new Population<>();
            Population<DoubleChromosome> childs = new Population<>();

            parents.add(individual);
            parents.add(msg.getIndividual());
            childs.setAs(parents);

            Crossover<DoubleChromosome> crossover = new OnePointCrossover<>(0.85);
            crossover.init(null);
            crossover.process(parents, childs);

            for (Individual<DoubleChromosome> child : childs.getIndividuals()) {
                master.tell(new IndivudalSpawn(child), getSelf());
            }
        }
    }

    private void takeAttack(Attack msg) {
        if (msg.getStrength() >= energy && energy > 0.0) {
            double damage = Math.min(energy * ENERGY_LOSS, energy);
            energy -= damage;
            getSender().tell(new Energy(damage), getSelf());
        }
    }

    private void pickupEnergy(Energy msg) {
        energy = Math.min(MAX_ENERGY, energy + msg.getEnergy());
    }
}
