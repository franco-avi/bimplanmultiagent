package ao.actors;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.japi.pf.ReceiveBuilder;
import ao.messages.EvolutionStart;
import ao.messages.IndividualAdd;
import ao.messages.IndivudalSpawn;
import ao.messages.Schedule;
import taskgraph.TaskGraph;

import java.util.HashSet;
import java.util.Set;

import static ao.Constants.*;


public class MasterActor extends AbstractActor {

    private TaskGraph taskGraph;

    private int bestPlanLength = Integer.MAX_VALUE;
    private String bestSchedule = "";

    private int generationCounter = 0;

    public static Props props() {
        return Props.create(MasterActor.class);
    }

    @Override
    public void preStart() {
        taskGraph = new TaskGraph(DATASET);

        Set<ActorRef> individuals = new HashSet<>();
        for (int j = 0; j < POPULATION_SIZE; j++) {
            ActorRef individual = getContext().actorOf(IndividualActor.props(taskGraph));
            individuals.add(individual);
        }

        for (ActorRef individual : individuals) {
            for (ActorRef neighbour : individuals) {
                individual.tell(new IndividualAdd(neighbour), getSelf());
            }
        }

        for (ActorRef individual : individuals) {
            individual.tell(new EvolutionStart(), getSelf());
        }
    }

    @Override
    public Receive createReceive() {
        ReceiveBuilder receiveBuilder = receiveBuilder();
        receiveBuilder.match(IndivudalSpawn.class, this::spawnIndividual);
        receiveBuilder.match(Schedule.class, this::updateBestPlan);

        return receiveBuilder.build();
    }

    private void spawnIndividual(IndivudalSpawn msg) {
        ActorRef child = getContext().actorOf(IndividualActor.props(msg.getIndividual(), taskGraph));
        child.tell(new EvolutionStart(), getSelf());
    }

    private void updateBestPlan(Schedule msg) {
        generationCounter++;

        if (msg.getScheduleLength() < bestPlanLength) {
            bestPlanLength = msg.getScheduleLength();
            bestSchedule = msg.getSchedule();
        }

        //System.out.printf("SCORE: %d, PLAN: %s\n", msg.getScheduleLength(), msg.getSchedule());

        if (generationCounter == POPULATION_SIZE + GENERATIONS_LIMIT) {
            System.out.printf("SCORE: %d, PLAN: %s\n", bestPlanLength, bestSchedule);
            getContext().system().terminate();
        }
    }
}
