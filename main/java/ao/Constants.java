package ao;

public class Constants {

     //public static final String DATASET = "/Users/Franco/IdeaProjects/ResearchProject/src/main/resources/chain101.csv";
    public static final String DATASET = "/Users/Franco/IdeaProjects/ResearchProject/src/main/resources/industrial.csv";

    public static final int POPULATION_SIZE = 10;
    public static final int GENERATIONS_LIMIT = 100;

    public static final double DEATH_TRESHOLD = 20.0;
    public static final double DEATH_PROB = 0.1;

    public static final double ATTACK_TRESHOLD = 5.0;
    public static final double ATTACK_PROB = 0.7;

    public static final double REPRODUCTION_TRESHOLD = 30;
    public static final double REPRODUCTION_PROB = 0.2;

    public static final double MAX_ENERGY = 100.0;
    public static final double MAX_START_ENERGY = 50.0;
    public static final double ENERGY_LOSS = 0.1;
}
