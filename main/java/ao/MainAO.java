package ao;

import akka.actor.ActorSystem;
import ao.actors.MasterActor;

public class MainAO {

    public static void main(String[] args) {
        ActorSystem system = ActorSystem.create("ao-system");
        system.actorOf(MasterActor.props());
    }
}
