import akka.actor.ActorSystem;
import jenes.GeneticAlgorithm;
import jenes.chromosome.DoubleChromosome;
import jenes.population.Fitness;
import jenes.population.Individual;
import jenes.population.Population;
import jenes.stage.AbstractStage;
import jenes.stage.operator.common.OnePointCrossover;
import jenes.stage.operator.common.TournamentSelector;
import taskgraph.TaskGraph;

public class MainMasterSlave {

    private static final String DATASET = "/Users/Franco/IdeaProjects/ResearchProject/src/main/resources/layer103.csv";
    // private static final String DATASET = "/Users/Franco/IdeaProjects/ResearchProject/src/main/resources/industrial.csv";

    private static final int POPULATION_SIZE = 1000;
    private static final int TOURNAMENT_SIZE = 10;
    private static final int ELITISM_SIZE = 1;
    private static final int GENERATION_LIMIT = 500;
    private static final int SLAVES = 3;

    public static void main(String[] args) {
        long time = System.currentTimeMillis();
        ActorSystem system = ActorSystem.create("master-system");

        TaskGraph taskGraph = new TaskGraph(DATASET);
        System.out.println(taskGraph);

        for (int s = 0; s < SLAVES; s++) {
            DoubleChromosome chromosome = new DoubleChromosome(taskGraph.getTaskNumber(), 0, 1);
            Individual<DoubleChromosome> sample = new Individual<>(chromosome);
            Population<DoubleChromosome> population = new Population<>(sample);
            GeneticAlgorithm<DoubleChromosome> ga = new GeneticAlgorithm<>(population, POPULATION_SIZE);
            Fitness<DoubleChromosome> fit = new Fitness<DoubleChromosome>(1, false) {

                @Override
                public void evaluate(Individual<DoubleChromosome> individual) {
                    double[] score = new double[1];
                    score[0] = taskGraph.getSchedule(individual).getFinishTime();
                    individual.setScore(score);
                }
            };

            AbstractStage<DoubleChromosome> selector = new TournamentSelector<>(TOURNAMENT_SIZE);
            AbstractStage<DoubleChromosome> crossover = new OnePointCrossover<>(0.85);

            ga.setFitness(fit);
            ga.addStage(selector);
            ga.addStage(crossover);
            ga.setElitism(ELITISM_SIZE);
            ga.setGenerationLimit(GENERATION_LIMIT);

            ga.evolve();

            Population.Statistics stats = ga.getLastPopulation().getStatistics();
            Individual best = stats.getGroup(Population.BEST).get(0);

            System.out.println("SCORE: " + best.getScore());
            System.out.println("PLAN: " + taskGraph.getSchedule(best));
            System.out.println("**************************************");

            /*for (Individual ind : ga.getLastPopulation()) {
                System.out.println("SCORE: " + ind.getScore());
                System.out.println("PLAN: " + taskGraph.getSchedule(ind));
                System.out.println("---------------------------------");
            }
            Utils.printStatistics(stats);*/
        }
        System.out.println(-time + System.currentTimeMillis());
        system.terminate();
    }
}