package taskgraph;

public class PriorityTask extends Task implements Comparable {

    private double priority;

    public PriorityTask(Task task, double priority) {
        super(task.getId(), task.getDuration(), task.getResources(), task.getDependencies());
        this.priority = priority;
    }

    public double getPriority() {
        return priority;
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof PriorityTask)) throw new ClassCastException();
        if (this.getId() == ((PriorityTask) o).getId()) return 0;
        return Double.compare(getPriority(), ((PriorityTask) o).getPriority());
    }
}
