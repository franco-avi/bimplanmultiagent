package taskgraph;

public class ScheduledTask extends Task implements Comparable {

    private int startTime;

    public ScheduledTask(Task task, int startTime) {
        super(task.getId(), task.getDuration(), task.getResources(), task.getDependencies());
        this.startTime = startTime;
    }

    public ScheduledTask(Task task) {
       this(task, 0);
    }


    public int getStartTime() {
        return startTime;
    }

    public int getEndTime() {
        return startTime + getDuration();
    }

    boolean isActive(int time) {
        return (time >= getStartTime() && time < getEndTime());
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof ScheduledTask)) throw new ClassCastException();
        if (this.getId() == ((ScheduledTask) o).getId()) return 0;
        return Integer.compare(getEndTime(), ((ScheduledTask) o).getEndTime());
    }

    @Override
    public String toString(){
        return String.format("StartTime: %d, EndTime: %d, TaskId: {%s}", getStartTime(), getEndTime(), getId());
    }
}
