package taskgraph;

import java.util.Set;

public class Schedule {

    private Set<ScheduledTask> tasks;
    private int finishTime;

    public Schedule(Set<ScheduledTask> tasks, int finishTime) {
        this.tasks = tasks;
        this.finishTime = finishTime;
    }

    public Set<ScheduledTask> getTasks() {
        return tasks;
    }

    public int getFinishTime() {
        return finishTime;
    }

    @Override
    public String toString(){
        return tasks.toString();
    }
}
