package taskgraph;

import jenes.chromosome.DoubleChromosome;
import jenes.population.Individual;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class TaskGraph {

    private Set<Task> tasks;
    private Map<String, Integer> resources;

    public TaskGraph(String filename){
        resources = new TreeMap<>();
        tasks = new HashSet<>();

        try (Scanner fileScanner = new Scanner(new File(filename))) {
            fileScanner.useDelimiter("\n");

            Map<Integer, String> resOrder = new TreeMap<>();
            try (Scanner resourceScanner = new Scanner(fileScanner.next())) {
                resourceScanner.useDelimiter(";");

                int order = 0;
                while (resourceScanner.hasNext()){
                    String resName = resourceScanner.next();
                    int resAmount = resourceScanner.nextInt();

                    resources.put(resName, resAmount);
                    resOrder.put(order, resName);
                    order++;
                }
            }

            while (fileScanner.hasNext()){
                String line = fileScanner.next();

                try (Scanner lineScanner = new Scanner(line)) {
                    lineScanner.useDelimiter(";");

                    int taskId = lineScanner.nextInt();
                    int taskDuration = lineScanner.nextInt();

                    Map<String, Integer> taskResources = new TreeMap<>();
                    for (int i = 0; i < resources.size(); i++) {
                        taskResources.put(resOrder.get(i), lineScanner.nextInt());
                    }

                    Set<Integer> taskDependencies = new TreeSet<>();
                    while (lineScanner.hasNext()){
                        taskDependencies.add(lineScanner.nextInt());
                    }

                    Task task = new Task(taskId, taskDuration, taskResources, taskDependencies);
                    tasks.add(task);
                }
            }

        } catch (FileNotFoundException e) {
            System.err.println("Error reading csv file");
        }
    }

    public int getResourceNumber(){
        return resources.size();
    }

    public int getTaskNumber(){
        return tasks.size();
    }


    public Schedule getSchedule(Individual<DoubleChromosome> individual) {

        Set<ScheduledTask> scheduledTasks = new HashSet<>();
        PriorityQueue<PriorityTask> satisfiedTasks = new PriorityQueue<>();
        PriorityQueue<PriorityTask> unsatisfiedTasks = new PriorityQueue<>();

        for (Task task : tasks){
            double[] priorities = individual.getChromosome().getValues();

            if (checkDependencies(task, scheduledTasks)) {
                satisfiedTasks.add(new PriorityTask(task, priorities[task.getId() - 1]));
            } else {
                unsatisfiedTasks.add(new PriorityTask(task, priorities[task.getId() - 1]));
            }
        }

        int finishTime = 0;
        while (scheduledTasks.size() < individual.getChromosomeLength()) {

            Task winner = satisfiedTasks.poll();
            int startTime = checkStartTime(winner, scheduledTasks);
            scheduledTasks.add(new ScheduledTask(winner, startTime));

            finishTime = Math.max(finishTime, startTime + winner.getDuration());

            Iterator<PriorityTask> iterator = unsatisfiedTasks.iterator();
            while (iterator.hasNext()){
                PriorityTask unsatisfiedTask = iterator.next();

                if (checkDependencies(unsatisfiedTask, scheduledTasks)) {
                    satisfiedTasks.add(unsatisfiedTask);
                    iterator.remove();
                }
            }
        }

        return new Schedule(scheduledTasks, finishTime);
    }

    private int checkStartTime(Task winner, Set<ScheduledTask> scheduledTasks) {
        int minStartTime = 0;
        Set<Integer> dependencies = winner.getDependencies();
        for (ScheduledTask scheduledTask : scheduledTasks) {
            if (dependencies.contains(scheduledTask.getId())){
                minStartTime = Math.max(minStartTime, scheduledTask.getEndTime());
            }
        }

        if (minStartTime == 0 && checkResources(winner, scheduledTasks, 0)){
            return 0;
        }

        int schedulingTime = Integer.MAX_VALUE;
        for (ScheduledTask scheduledTask : scheduledTasks) {
            int endTime = scheduledTask.getEndTime();

            if (endTime >= minStartTime && checkResources(winner, scheduledTasks, endTime)) {
                schedulingTime = Math.min(schedulingTime, endTime);
            }
        }

        return schedulingTime;
    }

    private boolean checkResources(Task task, Set<ScheduledTask> scheduledTasks, int time) {
        Map<String, Integer> usedResources = new HashMap<>();
        for (String resource: resources.keySet()) {
            usedResources.put(resource, task.getResources().get(resource));
        }

        for (ScheduledTask scheduledTask : scheduledTasks) {
            if (scheduledTask.isActive(time)) {
                for (String resource: resources.keySet()) {
                    usedResources.put(resource,
                            usedResources.get(resource) + scheduledTask.getResources().get(resource));
                }
            }
        }

        for (String resource: resources.keySet()) {
            if (usedResources.get(resource) > resources.get(resource)){
                return false;
            }
        }
        return true;
    }

    public int getSequentialDuration(){
        int seqDuration = 0;
        for (Task task: tasks) {
            seqDuration += task.getDuration();
        }
        return seqDuration;
    }

    public int getParallelDuration() {
        int maxDuration = 0;
        for (Task task: tasks) {
            maxDuration = Math.max(task.getDuration(), maxDuration);
        }
        return maxDuration;
    }

    private boolean checkDependencies(Task task, Collection<ScheduledTask> scheduledTasks){
        for (int dependency : task.getDependencies()) {
            if (!scheduledTasks.contains(new ScheduledTask(new Task(dependency)))){
                return false;
            }
        }
        return true;
    }

    public String toString(){
        String result = String.format("Res: %s\n", resources);
        for (Task task : tasks){
            result += task + "\n";
        }
        return result;
    }
}