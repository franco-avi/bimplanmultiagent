package taskgraph;

import java.util.*;

public class Task implements Comparable {

    private int id;
    private int duration;
    private Map<String, Integer> resources;
    private Set<Integer> dependencies;

    public Task(int id, int duration, Map<String, Integer> resources, Set<Integer> dependencies) {
        this.id = id;
        this.duration = duration;
        this.resources = resources;
        this.dependencies = dependencies;
    }

    public Task(int id, int duration, Map<String, Integer> resources){
        this(id, duration, resources, new TreeSet<>());
    }

    public Task(int id){
        this(id, 0, null);
    }

    public int getId() {
        return id;
    }

    public boolean isRoot(){
        return (dependencies.size() == 0);
    }

    public int getDuration() {
        return duration;
    }

    public Map<String, Integer> getResources() {
        return resources;
    }

    public Set<Integer> getDependencies() {
        return dependencies;
    }

    public String toString(){
        return String.format("ID: %d, Dur: %d, Res: %s, Dep: %s",
                id, duration, resources.toString(), dependencies.toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;
        return id == task.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public int compareTo(Object o) {
        if (!(o instanceof Task)) throw new ClassCastException();
        return Integer.compare(getId(), ((Task) o).getId());
    }
}
